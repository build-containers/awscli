FROM python:3.7-alpine

ARG pipcmd="awscli"

RUN pip install --upgrade $pipcmd

ENTRYPOINT ["/bin/bash"]